<img src="https://cdn.pixabay.com/photo/2014/04/03/11/08/tea-311845_960_720.png" width="180">

# Guía introductoria de POO con Java
Este proyecto contiene los ejercicios de la primera guía programación orientada a objectos con Java.
## Distribución de los paquetes del proyecto
* Paquete `main`: contiene la clase principal a partir del cual llamaremos al resto de los ejercicios.
* Paquete `services`: contiene subpaquetes con el nombre de cada ejercicio de la guía en formato **ejercio00** y
todos los métodos de las clases generadas, sus correspondientes *main class* y otras clases con métodos auxiliares.
* Paquete `exercises`: contiene subpaquetes con el nombre de cada ejercicio de la guía en formato **ejercio00** y
las clases necesarias para cada ejercicio.

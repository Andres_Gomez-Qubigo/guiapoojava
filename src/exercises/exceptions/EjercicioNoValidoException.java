package exercises.exceptions;

public class EjercicioNoValidoException extends Exception {
    public EjercicioNoValidoException() {
    }

    public EjercicioNoValidoException(String message) {
        super(message);
    }
}

package exercises.ejercicio14;

public class Persona {
    private String nombre;
    private Integer edad;
    private Character sexo;
    private Float peso;
    private Float altura;

    public Persona() {
        this.nombre = "";
        this.edad = 0;
        this.sexo = 0;
        this.peso = 0f;
        this.altura = 0f;
    }

    public Persona(String nombre, Integer edad, Character sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = 0f;
        this.altura = 0f;
    }

    public Persona(String nombre, Integer edad, Character sexo, Float peso, Float altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Float getAltura() {
        return altura;
    }

    public void setAltura(Float altura) {
        this.altura = altura;
    }
}

package exercises.ejercicio13;

import java.util.ArrayList;

public class AhorcadoLetra {
    private ArrayList<Integer> positions;
    private Boolean found;

    public ArrayList<Integer> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<Integer> positions) {
        this.positions = positions;
    }

    public Boolean getFound() {
        return found;
    }

    public void setFound(Boolean found) {
        this.found = found;
    }

    public AhorcadoLetra(ArrayList<Integer> positions, Boolean found) {
        this.positions = positions;
        this.found = found;
    }

    public AhorcadoLetra() {
        this.positions = new ArrayList<>();
    }

    public AhorcadoLetra(Boolean found) {
        this.positions = new ArrayList<>();
        this.found = found;
    }
}

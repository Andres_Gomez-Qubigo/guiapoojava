package exercises.ejercicio13;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ahorcado {
    private String word;
    private Integer maxPlays;
    private final Map<String, AhorcadoLetra> hungLetter;

    public Ahorcado(String word, Integer maxPlays) {
        this.word = word;
        this.maxPlays = maxPlays;
        this.hungLetter = new HashMap<String, AhorcadoLetra>();
        AhorcadoLetra auxAhorcadoLetra;
        String auxString;
        ArrayList<Integer> auxPositions;
        char auxChar;
        for (int i = 0; i < this.word.length(); i++) {
            auxChar = this.word.charAt(i);
            auxString = Character.toString(auxChar);
            if (!this.hungLetter.containsKey(auxString)) {
                auxPositions = new ArrayList<Integer>();
                auxPositions.add(i);
                auxAhorcadoLetra = new AhorcadoLetra(auxPositions, false);
                this.hungLetter.put(auxString, auxAhorcadoLetra);
            } else {
                this.hungLetter.get(auxString).getPositions().add(i);
            }
        }
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getMaxPlays() {
        return maxPlays;
    }

    public void setMaxPlays(Integer maxPlays) {
        this.maxPlays = maxPlays;
    }

    public Map<String, AhorcadoLetra> getHungLetter() {
        return hungLetter;
    }
}

package exercises.ejercicio16;

public class Matematica {
    private Float numero1, numero2;

    public Matematica() {
        this.numero1 = 0f;
        this.numero2 = 0f;
    }

    public Matematica(Float numero1, Float numero2) {
        this.numero1 = numero1;
        this.numero2 = numero2;
    }

    public Float getNumero1() {
        return numero1;
    }

    public void setNumero1(Float numero1) {
        this.numero1 = numero1;
    }

    public Float getNumero2() {
        return numero2;
    }

    public void setNumero2(Float numero2) {
        this.numero2 = numero2;
    }
}

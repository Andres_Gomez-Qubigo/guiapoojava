package exercises.ejercicio17;

public class Meses {
    private final String[] meses = new String[]{"enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto",
    "septiembre", "octubre", "noviembre", "diciembre"};
    private final String mesSecreto = meses[(int)(Math.random() * 12)];

    public Meses() {
    }

    public String getMesSecreto() {
        return mesSecreto;
    }
}

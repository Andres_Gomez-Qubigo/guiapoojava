package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

import exercises.exceptions.EjercicioNoValidoException;
import services.ejercicio13.Ejercicio13Main;
import services.ejercicio14.Ejercicio14Main;
import services.ejercicio15.Ejercicio15Main;
import services.ejercicio16.Ejercicio16Main;
import services.ejercicio17.Ejercicio17Main;
import services.ejercicio18.Ejercicio18Main;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> ejericiosDisponibles = new ArrayList<>(Arrays.asList(666, 13, 14, 15, 16, 17, 18));
        String ejercicios = "\nEjercicios disponibles: " + ejericiosDisponibles.toString();
        int rta;
        boolean salida = false;
        Scanner sc = new Scanner(System.in);
        while (!salida) {
            System.out.println("\n¿Que ejercicio desea ejecutar?");
            System.out.println(ejercicios + " (666 para salir)");
            try {
                rta = sc.nextInt();
                if (!ejericiosDisponibles.contains(rta)) {
                    throw new EjercicioNoValidoException();
                } else {
                    switch (rta) {
                        case 13:
                            Ejercicio13Main.ejercicio13Main();
                            break;
                        case 14:
                            Ejercicio14Main.ejercicio14Main();
                            break;
                        case 15:
                            Ejercicio15Main.ejercicio15Main();
                            break;
                        case 16:
                            Ejercicio16Main.ejercicio16Main();
                            break;
                        case 17:
                            Ejercicio17Main.ejercicio17Main();
                            break;
                        case 18:
                            Ejercicio18Main.ejercicio18Main();
                            break;
                        case 666:
                            salida = true;
                            break;
                    }
                }
            } catch (InputMismatchException e) {
                System.out.println("ERROR: Tipo de dato incorrecto, ingrese un entero de la lista...");
            } catch (EjercicioNoValidoException e) {
                System.out.println("ERROR: Ingrese un ejercicio que este dentro de la lista por favor...");
            } finally {
                sc.nextLine();
            }
        }
    }
}

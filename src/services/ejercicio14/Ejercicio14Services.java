package services.ejercicio14;

public class Ejercicio14Services {
    public static int calcularMC(Float peso, Float altura) {
        final double aux = peso / Math.pow(altura, 2);
        if (aux < 20) return -1;
        else if (aux >= 20 && aux <= 25) return 0;
        else return 1;
    }

    public static boolean esMayorDeEdad(Integer edad) {
        return edad > 17;
    }

    public static void comprobarSexo(Character sexo) {
        if (sexo == 'M' || sexo == 'H' || sexo == 'O') System.out.println("Dato correcto.");
        else System.out.println("Dato incorrecto, deber ser M, H u O.");
    }

    public static String traducirMC(int mc) {
        String aux = "";
        switch (mc) {
            case -1:
                aux = "Peso ideal";
                break;
            case 0:
                aux = "Debajo del peso ideal";
                break;
            case 1:
                aux = "Sobrepeso";
                break;
        }
        return aux;
    }

    public static String traducirMayorDeEdad(boolean mayoria) {
        String aux;
        if (mayoria) {
            aux = "Si";
        } else aux = "No";
        return aux;
    }
}

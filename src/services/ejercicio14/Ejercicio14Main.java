package services.ejercicio14;

import exercises.ejercicio14.Persona;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Ejercicio14Main {
    public static void ejercicio14Main() {
        System.out.println("\n############### Ejercicio 14 ##############");
        System.out.println("################# Persona #################\n");

        String nombre;
        int edad;
        char sexo;
        float peso, altura;
        Scanner sc = new Scanner(System.in);
        Persona persona1, persona2, persona3;
        Persona[] personas;
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        // carga de datos usuario 1
        System.out.println("###### Carga de datos usuario 1 ######");
        System.out.println("Ingrese su nombre:");
        nombre = sc.next();
        System.out.println("Ingrese su edad:");
        edad = sc.nextInt();
        System.out.println("Ingrese su sexo (M, H u O):");
        sexo = sc.next().toUpperCase().charAt(0);
        Ejercicio14Services.comprobarSexo(sexo);
        System.out.println("Ingrese su peso (kg):");
        peso = sc.nextFloat();
        System.out.println("Ingrese su altura (m):");
        altura = sc.nextFloat();
        persona1 = new Persona(nombre, edad, sexo, peso, altura);

        // carga de datos usuario 2
        System.out.println("\n###### Carga de datos usuario 2 ######");
        System.out.println("Ingrese su nombre:");
        nombre = sc.next();
        System.out.println("Ingrese su edad:");
        edad = sc.nextInt();
        System.out.println("Ingrese su sexo (M, H u O):");
        sexo = sc.next().toUpperCase().charAt(0);
        Ejercicio14Services.comprobarSexo(sexo);
        persona2 = new Persona(nombre, edad, sexo);
        persona2.setPeso(150 - (float)Math.random() * 100);
        persona2.setAltura(2 - (float)Math.random() * 0.51f);

        // carga de datos usuario 3
        persona3 = new Persona();
        System.out.println("\n###### Carga de datos usuario 3 ######");
        System.out.println("Ingrese su nombre:");
        persona3.setNombre(sc.next());
        System.out.println("Ingrese su edad:");
        persona3.setEdad(sc.nextInt());
        System.out.println("Ingrese su sexo (M, H u O):");
        persona3.setSexo(sc.next().toUpperCase().charAt(0));
        Ejercicio14Services.comprobarSexo(persona3.getSexo());
        System.out.println("Ingrese su peso (kg):");
        persona3.setPeso(sc.nextFloat());
        System.out.println("Ingrese su altura (m):");
        persona3.setAltura(sc.nextFloat());

        // se incializan los objetos del array para el prcceso de impresion de informacion
        personas = new Persona[]{persona1, persona2, persona3};

        // se imprime la info de cada usuario
        for (int i = 0; i < personas.length; i++) {
            System.out.println("\n###### Datos de usuario " + (i + 1) + " ######");
            int auxint = Ejercicio14Services.calcularMC(personas[i].getPeso(), personas[i].getAltura());
            boolean auxBool = Ejercicio14Services.esMayorDeEdad(personas[i].getEdad());
            System.out.println("Nombre: " + personas[i].getNombre() + "\nEdad: " + personas[i].getEdad() + "\nSexo: " +
                    personas[i].getSexo() + "\nPeso: " + df.format(personas[i].getPeso()) + " kg" + "\nAltura: " +
                    df.format(personas[i].getAltura()) + " m");
            System.out.println("MC: " + Ejercicio14Services.traducirMC(auxint));
            System.out.println("¿Es mayor de edad? - " + Ejercicio14Services.traducirMayorDeEdad(auxBool));
        }
    }
}

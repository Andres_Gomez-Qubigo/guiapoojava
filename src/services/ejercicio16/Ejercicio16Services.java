package services.ejercicio16;

import exercises.ejercicio16.Matematica;

public class Ejercicio16Services {
    public static Float devolverMayor(Matematica matematica) {
        Float auxFloat;
        if (matematica.getNumero1() > matematica.getNumero2()) {
            auxFloat = matematica.getNumero1();
        } else auxFloat = matematica.getNumero2();
        return auxFloat;
    }

    public static Float calcularPotencia(Matematica matematica) {
        Float auxMenor, auxMayor;
        if (matematica.getNumero1() > matematica.getNumero2()) {
            auxMayor = matematica.getNumero1();
            auxMenor = matematica.getNumero2();
        } else {
            auxMayor = matematica.getNumero2();
            auxMenor = matematica.getNumero1();
        }
        return Math.round(Math.pow(auxMayor, auxMenor) * 100f) / 100f;
    }

    public static Float calcularRaiz(Matematica matematica) {
        Float auxFloat;
        if (matematica.getNumero1() < matematica.getNumero2()) {
            auxFloat = matematica.getNumero1();
        } else auxFloat = matematica.getNumero2();
        if (auxFloat < 0) auxFloat *= -1;
        return (float)Math.pow(auxFloat, (1f/2f));
    }

    public static Float sumarAngulos(Matematica matematica) {
        Float auxNum1 = matematica.getNumero1(), auxNum2 = matematica.getNumero2();
        return (float)(Math.sin(auxNum1) * Math.cos(auxNum2) + Math.cos(auxNum1) + Math.sin(auxNum2));
    }
}

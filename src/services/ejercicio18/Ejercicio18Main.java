package services.ejercicio18;

public class Ejercicio18Main {
    public static void ejercicio18Main() {
        System.out.println("\n############### Ejercicio 18 ##############");
        System.out.println("################## Arrays #################\n");

        float[] arreglo1 = new float[50], arreglo2 = new float[20];

        arreglo1 = Ejercicio18Services.randomizeArray(arreglo1);
        arreglo2 = Ejercicio18Services.randomizeArray(arreglo2);

        System.out.println("============ ARREGLO 1 ============ [ORIGINAL]");
        for (float v : arreglo1) System.out.println(v);
        System.out.println("============ ARREGLO 2 ============ [ORIGINAL]");
        for (float v : arreglo2) System.out.println(v);

        System.out.println("============ ARREGLO 1 ============ [ORDENADO]");
        Ejercicio18Services.ordenarArreglo(arreglo1);
        for (float v : arreglo1) System.out.println(v);

        System.out.println("============ ARREGLO 2 ============ [MODIFICADO]");
        arreglo2 = Ejercicio18Services.remplazarArreglo(arreglo1, arreglo2);
        for (float v : arreglo2) System.out.println(v);
    }
}

package services.ejercicio18;

import java.util.Arrays;

public class Ejercicio18Services {
    public static float[] randomizeArray(float[] arreglo) {
        float[] auxFloat = new float[arreglo.length];
        for (int i = 0; i < auxFloat.length; i++) {
            auxFloat[i] = ((float)Math.random() * 1000);
        }
        return auxFloat;
    }

    public static void ordenarArreglo(float[] arreglo) {
        boolean ordenado;
        do {
            ordenado = true;
            for (int i = 0; i < (arreglo.length - 1); i++) {
                if (arreglo[i] > arreglo[i + 1]) {
                    ordenado = false;
                    float auxNum = arreglo[i];
                    arreglo[i] = arreglo[i + 1];
                    arreglo[i + 1] = auxNum;
                }
            }
        } while (!ordenado);
    }

    public static float[] remplazarArreglo(float[] arreglo1, float[] arreglo2) {
        float[] auxFloat = arreglo2;
        auxFloat = Arrays.copyOfRange(arreglo1, 0, 20);
        Arrays.fill(auxFloat, 9, 20, 0.5f);
        return auxFloat;
    }
}

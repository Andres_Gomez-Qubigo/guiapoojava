package services.ejercicio17;

import exercises.ejercicio17.Meses;

import java.util.Scanner;

public class Ejercicio17Main {
    public static void ejercicio17Main() {
        System.out.println("\n############### Ejercicio 16 ##############");
        System.out.println("################### Meses #################\n");

        String rta;
        Scanner sc = new Scanner(System.in);
        Meses arreglo = new Meses();

        while (true) {
            System.out.println("Adivine el mes secreto. Introduzca el nombre del mes:");
            rta = sc.nextLine();
            if (Ejercicio17Services.acierto(arreglo, rta)) {
                System.out.println("Ha encontrado el mes oculto!\n");
                break;
            } else {
                System.out.println("No ha acertado. Intente nuevamente...\n");
            }
        }
    }
}

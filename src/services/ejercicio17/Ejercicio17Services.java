package services.ejercicio17;

import exercises.ejercicio17.Meses;

public class Ejercicio17Services {
    public static boolean acierto(Meses arreglo, String mes) {
        return mes.equals(arreglo.getMesSecreto().toLowerCase());
    }
}

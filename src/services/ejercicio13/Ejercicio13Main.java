package services.ejercicio13;

import exercises.ejercicio13.Ahorcado;

import java.io.Console;
import java.util.Scanner;

public class Ejercicio13Main {
    public static void ejercicio13Main() {
        System.out.println("\n############### Ejercicio 13 ##############");
        System.out.println("################# Ahorcado ################\n");

        Ahorcado ahorcado;
        String palabra, letra;
        int jugadas;
        Scanner sc = new Scanner(System.in);
        Console console = System.console();

        do {
            System.out.println("Por favor, ingrese una palabra:");
            palabra = sc.nextLine();
        } while (palabra.length() < 1);
        do {
            System.out.println("Ingrese la cantida maxima de jugadas (mayor a 0):");
            jugadas = sc.nextInt();
        } while (jugadas < 1);
        ahorcado = new Ahorcado(palabra, jugadas);

        while (ahorcado.getMaxPlays() > 0 && !Ejercicio13Services.isComplete(ahorcado)) {
            Ejercicio13Services.showInfo(ahorcado);
            System.out.println("Ingrese una letra:");
            letra = sc.next();
            if (Ejercicio13Services.isLetterInWord(ahorcado, letra)) {
                if (!ahorcado.getHungLetter().get(letra).getFound()){
                    System.out.println("Parece que tuvo suerte...");
                    ahorcado.getHungLetter().get(letra).setFound(true);
                } else {
                    System.out.println("Creo que ya ha ingresado esa letra. Acomodese los anteojos...");
                }
            } else {
                System.out.println("Parece que se equivoco de tecla. Intentelo de nuevo...");
                ahorcado.setMaxPlays(ahorcado.getMaxPlays() - 1);
            }
        }
        if (Ejercicio13Services.isComplete(ahorcado)) {
            System.out.println("\nBueno, me ha sorprendido. Ha ganaddo... esta vez.");
        } else {
            System.out.println("\nHo, espere, se le han acabado las oportunidades. Lea un libro y vuelva.");
            System.out.println("\nLa palabra era [" + ahorcado.getWord() + "]");
        }
    }
}

package services.ejercicio13;

import exercises.ejercicio13.Ahorcado;
import exercises.ejercicio13.AhorcadoLetra;

import java.util.ArrayList;
import java.util.Map;

public class Ejercicio13Services {
    // metodo que retorna la longitud de la palabra
    public static int getWordLength(Ahorcado ahorcado) {
        return ahorcado.getWord().length();
    }

    // metodo para verficiar si una letra es parte de la palabra
    public static boolean isLetterInWord(Ahorcado ahorcado, String letter) {
        return ahorcado.getWord().contains(letter);
    }

    // metodo para informar de acierto de letra en la palabra en determinada posicion
    public static boolean isInPosition(Ahorcado ahorcado, Integer position, String letter) {
        return ahorcado.getWord().substring(position).equals(letter);
    }

    // metodo que muestra informacin de la partida como la cantidad de jugadas disponibles, las posiciones en las que
    // estan y la palabra en si con los lugares vacios y las letras descubiertas
    public static void showInfo(Ahorcado ahorcado) {
        ArrayList<Integer> auxInteger;
        StringBuilder auxString = new StringBuilder();
        System.out.println("\n###### Jugadas restantes: " + ahorcado.getMaxPlays() + " ######");
        System.out.println("Logitud de palabra : " + ahorcado.getWord().length());
        if (wordContainsTrue(ahorcado)) {
            System.out.println("Han sido encontradas las siguientes letras en las siguiente posiciones:");
            for (Map.Entry<String, AhorcadoLetra> entry: ahorcado.getHungLetter().entrySet()) {
                if (ahorcado.getHungLetter().get(entry.getKey()).getFound()) {
                    auxInteger = ahorcado.getHungLetter().get(entry.getKey()).getPositions();
                    auxInteger.replaceAll(j -> j + 1);
                    System.out.println(entry.getKey() + " -> " +
                            auxInteger.toString().replaceAll("[\\[\\]]", ""));
                }
            }
            for (int i = 0; i < ahorcado.getWord().length(); i++) {
                if (ahorcado.getHungLetter().get(Character.toString(ahorcado.getWord().charAt(i))).getFound()) {
                    auxString.append(ahorcado.getWord().charAt(i)).append(" ");
                } else {
                    auxString.append("_ ");
                }
            }
            System.out.println("\n" + auxString.toString() + "\n");
        } else {
            System.out.println("No se ha encontrado ninguna letra aun...");
        }
    }

    // metodo para saber si se encotro la palabra completa
    public static boolean isComplete(Ahorcado ahorcado) {
        for (Map.Entry<String, AhorcadoLetra> entry: ahorcado.getHungLetter().entrySet()) {
            if (!ahorcado.getHungLetter().get(entry.getKey()).getFound()) {
                return false;
            }
        }
        return true;
    }

    // metodo para saber si existe alguna letra que se haya adivinado
    private static boolean wordContainsTrue(Ahorcado ahorcado) {
        for (Map.Entry<String, AhorcadoLetra> entry: ahorcado.getHungLetter().entrySet()) {
            if (ahorcado.getHungLetter().get(entry.getKey()).getFound()) {
                return true;
            }
        }
        return false;
    }
}

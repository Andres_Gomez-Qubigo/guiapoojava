package services.ejercicio15;

import exercises.ejercicio15.Cadena;

import java.util.HashMap;
import java.util.Map;

public class Ejercicio15Services {
    public static void mostrarVocales(Cadena cadena) {
        HashMap<Character, Integer> vocales = new HashMap<Character, Integer>(){{
            put('a', 0);
            put('e', 0);
            put('i', 0);
            put('o', 0);
            put('u', 0);}};
        for (int i = 0; i < cadena.getFrase().length(); i++) {
            char auxChar = cadena.getFrase().toLowerCase().charAt(i);
            if (vocales.containsKey(auxChar)) {
                vocales.put(auxChar, (vocales.get(auxChar) + 1));
            }
        }
        System.out.println("\nLas vocales contadas en la frase son:");
        vocales.forEach((key1, value) -> System.out.println(key1 + ": " + value));
    }

    public static void invertirFrase(Cadena cadena) {
        StringBuilder auxString = new StringBuilder();
        String frase = cadena.getFrase();
        for (int i = (frase.length() - 1); i > -1; i--) {
            auxString.append(frase.charAt(i));
        }
        System.out.println("\nFrase invertida:\n" + auxString.toString());
    }

    public static void vecesRepetido(Cadena cadena, char letra) {
        int contador = 0;
        String frase = cadena.getFrase();
        for (int i = 0; i < frase.length(); i++) {
            if (frase.charAt(i) == letra) contador++;
        }
        System.out.println("<" + letra + "> se repite " + contador + " veces.");
    }

    public static void comparaLongitud(Cadena cadena, String nuevaFrase) {
        System.out.println("\nLa longitud de la primera frase es de [" + cadena.getLongitud() +
                "] y la de la segunda frase es de [" + nuevaFrase.length() + "].");
    }

    public static void unirFrases(Cadena cadena, String nuevaFrase) {
        String auxString = cadena.getFrase() + " " +  nuevaFrase;
        cadena.setFrase(auxString);
        System.out.println("Se han undio las 2 frases:\n" + cadena.getFrase());
    }

    public static void remplazar(Cadena cadena, char caracter) {
        String auxString = cadena.getFrase().replace('a', caracter);
        cadena.setFrase(auxString);
        System.out.println("Se replazan las letras <a> de la frase por el caracter <" + caracter + ">, queda:\n" +
                cadena.getFrase());
    }
}

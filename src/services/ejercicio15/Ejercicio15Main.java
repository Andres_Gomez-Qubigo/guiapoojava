package services.ejercicio15;

import exercises.ejercicio15.Cadena;

import java.util.Scanner;

public class Ejercicio15Main {
    public static void ejercicio15Main() {
        System.out.println("\n############### Ejercicio 15 ##############");
        System.out.println("################## Frase ##################\n");

        String auxString;
        char auxChar;
        Cadena cadena;
        Scanner sc = new Scanner(System.in);

        // se ingresa la frase
        System.out.println("Ingrese una frase:");
        auxString = sc.nextLine();
        cadena = new Cadena(auxString);

        // se muestran las vocales contadas y la frase invertida
        Ejercicio15Services.mostrarVocales(cadena);
        Ejercicio15Services.invertirFrase(cadena);

        // se solicita ingresar un caracter para contarlo y se muestra en pantalla
        System.out.println("\nIngrese una letra para contarla:");
        auxChar = sc.nextLine().charAt(0);
        Ejercicio15Services.vecesRepetido(cadena, auxChar);

        // se compara la longitud de la palabar de la frase y la de la palabra de la proporcionada
        System.out.println("\nIngrese una frase para comparar su longitud:");
        auxString = sc.nextLine();
        Ejercicio15Services.comparaLongitud(cadena, auxString);

        // se pasa una nueva frase a un metodo la unira con la frase de la clase Cadena
        System.out.println("\nIngrese una frase para unirla con la primera:");
        auxString = sc.nextLine();
        Ejercicio15Services.unirFrases(cadena, auxString);

        // se pasa un caracter para remplazar a la letra 'a' en la frase
        System.out.println("\nIngrese un caracter para remplazar la letra <a> en la frase:");
        auxChar = sc.nextLine().charAt(0);
        Ejercicio15Services.remplazar(cadena, auxChar);
    }
}
